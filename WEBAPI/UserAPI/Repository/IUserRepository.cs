﻿using UserAPI.Model;

namespace UserAPI.Repository
{
    public interface IUserRepository
    {
        int BlockUnblockuser(bool blockUnblockUser, User userExit);
        User GetUserById(int id);
        int DeleteUser(User userExist);
        List<User> GetAllUsers();
        User LogIn(string name, string password);
        User GetUserByName(string name);
        int RegisterUser(User user);
        int EditUser(User userExist);
    }
}
